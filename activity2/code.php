<?php 

class Building {
  protected $name;
  protected $floor;
  protected $address;

  public function __construct($name, $floor, $address) {

		$this->name = $name;
		$this->floor = $floor;
		$this->address = $address;
	}

  public function getName() {
    return $this->name;
  }

  public function getFloor() {
    return $this->floor;
  }

  public function getAddress() {
    return $this->address;
  }
  
  public function setName($name) {
		
		if(gettype($name) === "string"){
			$this->name = $name;
		}
	}

  public function setFloor($floor) {
    if (gettype($floor) === "int"){
    	$this->floor = $floor;
    }
  }

  public function setAddress($address) {
 		
 		if(gettype($address) === "string"){
			$this->address = $address;
		}
  }
}



class Condominium extends Building {

  public function __construct($name, $floor, $address) {
    parent::__construct($name, $floor, $address);
  }
}


$building = new Building('ads', 8, 'asd');



$condominium = new Condominium('asdasd', 5, 'asdasda');

