<?php require_once "./code.php"; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Activity 2</title>
</head>
<body>

	<h1>Building</h1>

	<?php $building->setName('Caswynn Building'); ?>
	<p> The name of the building is <?php echo $building->getName(); ?></p>

	<?php $building->setFloor(8); ?>
	<p> The <?php echo $building->getName(); ?> has <?php echo $building->getFloor(); ?> floors </p>

	<?php $building->setAddress('Timog Ave., Quezon City, Philippines.'); ?>
	<p> The <?php echo $building->getName(); ?> is located at <?php echo $building->getAddress(); ?> </p>

	<?php $building->setName('Caswynn Complex.'); ?>
	<p>The name of the building has been changed to <?php echo $building->getName(); ?> </p>

	<h1>Condominium</h1>

	<?php $condominium->setName('Enzo Condo.'); ?>
	<p>The name of the condominium is <?php echo $condominium->getName(); ?> </p>

	<?php $condominium->setFloor(5); ?>
	<p>The <?php echo $condominium->getName(); ?> has <?php echo $condominium->getFloor(); ?> floors  </p>

	<?php $condominium->setName('Enzo Condo.'); ?>
	<p>The name of the condominium is <?php echo $condominium->getName(); ?> </p>

	<?php $condominium->setAddress('Buendia Avenue, Makati City, Philippines.'); ?>
	<p>The <?php echo $condominium->getName(); ?> is located at <?php echo $condominium->getAddress(); ?> </p>

	<?php $condominium->setName('Enzo Tower'); ?>
	<p>The name of the condominium has been changed to <?php echo $condominium->getName(); ?> </p>



</body>
</html>